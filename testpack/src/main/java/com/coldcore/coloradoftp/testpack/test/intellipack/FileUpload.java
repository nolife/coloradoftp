package com.coldcore.coloradoftp.testpack.test.intellipack;

import com.coldcore.coloradoftp.testpack.test.BaseCommunicatorTest;
import org.apache.log4j.Logger;

/**
 * @see com.coldcore.coloradoftp.testpack.test.genericbundle.FileUpload
 */
public class FileUpload extends com.coldcore.coloradoftp.testpack.test.genericbundle.FileUpload {

  private static Logger log = Logger.getLogger(FileUpload.class);


  protected void setUp() throws Exception {
    super.setUp("intellipack-beans.xml");
  }

}