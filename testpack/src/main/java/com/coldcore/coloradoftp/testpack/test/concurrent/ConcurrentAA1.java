package com.coldcore.coloradoftp.testpack.test.concurrent;

import org.apache.log4j.Logger;

/**
 * Concurrent test with 20 users.
 */
public class ConcurrentAA1 extends ConcurrentBase {

    protected void setUp() throws Exception {
        this.setUp("concurrent-beans-plain.xml");
    }


    protected void setUp(String beansFilename) throws Exception {
        this.beansFilename = beansFilename;
        super.setUp();
    }


    public void test1() throws Exception {
        startClients();
        waitFor(60*3);
        stopClients();
        checkErrors();
    }

}