package com.coldcore.coloradoftp.testpack.test.genericbundle;

import com.coldcore.coloradoftp.testpack.test.BaseCommunicatorTest;
import org.apache.log4j.Logger;

/**
 * Test if server properly responds to SITE commands.
 * Testing SITE, SITE HELP commands
 */
public class SiteHelp extends BaseCommunicatorTest {

  private static Logger log = Logger.getLogger(SiteHelp.class);


  protected void setUp() throws Exception {
    beansFilename = "genericbundle-beans.xml";
    super.setUp();
  }


  public void testBasics() throws Exception {
    command("site", "530");
    command("site help", "530");
    anonymousLogin();
    command("site", "501");
    command("site help", "200");
    command("site xxxx", "500");
    logout();
  }
}
