package com.coldcore.coloradoftp.testpack.test;

import com.coldcore.coloradoftp.testpack.core.Communicator;
import com.coldcore.coloradoftp.testpack.core.PasvDataTransfer;
import com.coldcore.coloradoftp.testpack.core.PortDataTransfer;
import com.coldcore.misc5.CByte;
import net.sf.cotta.TFile;
import net.sf.cotta.TFileFactory;
import org.apache.log4j.Logger;

import java.util.StringTokenizer;

/**
 * This is a base for tests using the Communicator as an FTP client.
 * The class provides low-level FTP control but tests extending it may seem
 * more complex. For the general test cases it is recommended to extend
 * the BaseClientTest instead which is using the Apache FTP client.
 */
abstract public class BaseCommunicatorTest extends BaseNoClientTest {

  private static Logger log = Logger.getLogger(BaseCommunicatorTest.class);
  protected Communicator communicator;
  protected PasvDataTransfer pasvTransfer;
  protected PortDataTransfer portTransfer;


  protected void setUp() throws Exception {
    initializeFtpServer();
    startFtpServer();
    Thread.sleep(100);
    createRemoteContent();
    initializeFtpClient();
    connectFtpClient();
    Thread.sleep(100);
  }


  protected void tearDown() throws Exception {
    disconnectFtpClient();
    stopFtpServer();
    Thread.sleep(100);
  }


  /** Initialize FTP client */
  protected void initializeFtpClient() {
    if (communicator == null) {
      communicator = new Communicator(remoteHost, remotePort);
    }
  }


  /** Open connection to a server */
  protected void connectFtpClient() throws Exception {
    if (communicator == null) throw new Exception("No client");
    communicator.openConnection();
    String reply = communicator.nextReply();
    if (!reply.startsWith("220")) throw new Exception("Remote server did not accept connection: "+reply);
    log.info("Remote server accepted connection");
  }


  /** Close connection to a server */
  protected void disconnectFtpClient() throws Exception {
    if (communicator == null) throw new Exception("No client");
    communicator.closeConnection();
  }


  /** This login method should be used only in the setUp() */
  protected void loginFtpClient() throws Exception {
    if (communicator == null) throw new Exception("No client");
    communicator.send("user "+username);
    String reply = communicator.nextReply();
    if (!reply.startsWith("331")) throw new Exception("Remote server did not accept username: "+reply);
    communicator.send("pass "+password);
    reply = communicator.nextReply();
    if (!reply.startsWith("230")) throw new Exception("Remote server did not accept password: "+reply);
  }


  /*** Methods to use in tests ***/


  /** Anonymous login */
  protected void anonymousLogin() throws Exception {
    command("user anonymous", "331");
    command("pass ano@nym.ous", "230");
  }


  /** Login */
  protected void login(String username, String password) throws Exception {
    command("user "+username, "331");
    command("pass "+password, "230");
  }


  /** Logout */
  protected void logout() throws Exception {
    command("quit", "221");
    communicator.nextReply();
    if (communicator.isOpen()) fail("QUIT closes connection NO");
    log.info("QUIT closes connection OK");
  }


  /**
   * Send command to a server
   * @param input Command input
   * @param responseCode Expected response code
   * @return Server's reply or NULL if response codes do not match
   */
  protected String command(String input, String... responseCode) throws Exception {
    communicator.send(input);
    String reply = communicator.nextReply();
    for (String code : responseCode) {
      if (reply.equals(code) || reply.startsWith(code+" ") || reply.startsWith(code+"- ")) {
        log.info(input+" OK");
        return reply;
      }
    }
    fail(input+" NO: "+reply);
    return null;
  }


  /**
   * Get the next server reply code
   * @param responseCode Expected response code
   * @return Server's reply or NULL if response codes do not match
   */
  protected String nextReplyCode(String... responseCode) throws Exception {
    String reply = communicator.nextReply();
    for (String code : responseCode) {
      if (reply.equals(code) || reply.startsWith(code+" ") || reply.startsWith(code+"- ")) {
        log.info("Next reply OK");
        return reply;
      }
    }
    fail("Next reply NO: "+reply);
    return null;
  }


  /** Test if the current working directory is empty */
  protected void ensureNothingListedInWorkingDirectory() throws Exception {
    pasv();

    command("nlst", "150");

    String data = pasvTransfer.readDataAsString();
    if (data.length() > 0) fail("Listing NO: "+data);
    log.info("Listing OK");

    dataTransferComplete();
  }


  /** Test if the current working directory matches
   * @param dir Expected directory
   */
  protected void ensureWorkingDirectoryMatches(String dir) throws Exception {
    String pwd = printWorkingDirectory();
    if (!dir.equals(pwd)) fail("Current directory NO: "+pwd);
    log.info("Current directory OK");
  }


  /** Get current working directory
   * @return Working directory
   */
  protected String printWorkingDirectory() throws Exception {
    String reply = command("pwd", "257");
    int i1 = reply.indexOf("\"");
    int i2 = reply.lastIndexOf("\"");
    return reply.substring(i1+1, i2);
  }


  /**
   * Test if the current working directory listing contains cetrain paths
   * @param paths Paths that should not be in the listing
   */
  protected void ensureListedInWorkingDirectory(String... paths) throws Exception {
    pasv();

    command("nlst", "150");

    String data = pasvTransfer.readDataAsString();
    for (String path : paths)
      if (data.indexOf(path) == -1) fail("Listing NO: "+data);
    log.info("Listing OK");

    dataTransferComplete();
  }


  /**
   * Test if the current working directory listing does not contain certain paths
   * @param paths Paths that should not be in the listing
   */
  protected void ensureNotListedInWorkingDirectory(String... paths) throws Exception {
    pasv();

    command("nlst", "150");

    String data = pasvTransfer.readDataAsString();
    for (String path : paths)
      if (data.indexOf(path) != -1) fail("Listing NO: "+data);
    log.info("Listing OK");

    dataTransferComplete();
  }


  /** Switch to I mode */
  protected void switchToBinaryMode() throws Exception {
    command("type I", "200");
  }


  /** Switch to A mode */
  protected void switchToAsciiMode() throws Exception {
    command("type A", "200");
  }


  /** Initiate PASV data transfer */
  protected void pasv() throws Exception {
    String reply = command("pasv", "227");

    if (portTransfer != null && portTransfer.isConnected())
      fail("PORT data transfer already in progress");
    if (pasvTransfer != null && pasvTransfer.isConnected())
      fail("PASV data transfer already in progress");

    StringTokenizer st = new StringTokenizer(reply, "()");
    st.nextToken();
    String addr = st.nextToken();
    st = new StringTokenizer(addr, ",");
    String ip = st.nextToken()+"."+st.nextToken()+"."+st.nextToken()+"."+st.nextToken();
    int port = Integer.parseInt(st.nextToken())*256+Integer.parseInt(st.nextToken());
    log.info("PASV connection required to "+ip+":"+port);

    pasvTransfer = new PasvDataTransfer(ip, port);
  }


  /** Initiate PORT data transfer */
  protected void port() throws Exception {
    int port = 7001;
    int i = port/256;
    String pstr = i+","+(port-i*256);

    command("port 127,0,0,1,"+pstr, "200");

    if (portTransfer != null && portTransfer.isConnected())
      fail("PORT data transfer already in profress");
    if (pasvTransfer != null && pasvTransfer.isConnected())
      fail("PASV data transfer already in profress");

    log.info("PORT connection required to 127.0.0.1:"+port);

    portTransfer = new PortDataTransfer(7001);
    portTransfer.openConnection();
  }


  /** Read server reply on data transfer completion */
  protected void dataTransferComplete() throws Exception {
    if (portTransfer == null && pasvTransfer == null) fail("No data transfer object");

    String reply = communicator.nextReply();
    if (!reply.startsWith("226 ") && !reply.startsWith("250 ")) fail("Data transfer complete NO: "+reply);
    if (portTransfer != null && portTransfer.isBusy()) fail("PORT data transfer object is still busy");
    if (pasvTransfer != null && pasvTransfer.isBusy()) fail("PASV data transfer object is still busy");
    log.info("Data transfer complete OK");
  }


  /**
   * Test that a remote file has expected size
   * @param filename File name in an absolute form
   * @param size Expected file size
   */
  protected void checkRemoteFileSize(String filename, long size) throws Exception {
    TFileFactory fileFactory = getFileFactory();
    long fsize = fileFactory.file(filename).length();
    if (fsize != size) fail("File size does not match ("+fsize+"/"+size+")");
    log.info("File size OK");
  }


  /**
   * Test if a remote binary file contains expected data (all bytes match)
   * @param filename File name in an absolute form
   * @param bdata Expected file data
   */
  protected void checkRemoteBinaryFileIntegrity(String filename, byte[] bdata) throws Exception {
    TFileFactory fileFactory = getFileFactory();
    TFile tfile = fileFactory.file(filename);
    byte[] data = CByte.toByteArray(tfile.io().inputStream());
    if (data.length != bdata.length) fail("File size does not match ("+data.length+"/"+bdata.length+")");

    for (int z = 0; z < data.length; z++)
      if (data[z] != bdata[z]) fail("File content does not match");

    log.info("File integrity OK");
  }

}