package com.coldcore.coloradoftp.testpack.test;

import com.coldcore.coloradoftp.factory.ObjectFactory;
import com.coldcore.coloradoftp.testpack.core.Launcher;
import com.coldcore.misc5.CByte;
import com.coldcore.misc5.StringReaper;
import junit.framework.TestCase;
import net.sf.cotta.TDirectory;
import net.sf.cotta.TFile;
import net.sf.cotta.TFileFactory;
import net.sf.cotta.io.OutputMode;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.core.io.DefaultResourceLoader;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;

import java.io.ByteArrayInputStream;

/**
 * This is a base for test files using their own FTP client.
 */
abstract public class BaseNoClientTest extends TestCase {

  private static Logger log = Logger.getLogger(BaseNoClientTest.class);
  protected String username = "anonymous";
  protected String password = "ano@nym.ous";
  protected String remoteHost = "127.0.0.1";
  protected int remotePort = 2121;
  protected String remoteUsersPath = "/ftp/users";
  protected String fileFactoryName = "cottaFileFactory";
  protected String beansFilename = "beans.xml";
  protected Launcher launcher;

  protected static final long MBx1 = 1024L*1024L; //1 MB

  protected String lorem = "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, " +
              "sed diam nonumy eirmod tempor invidunt ut labore et dolore magna " +
              "aliquyam erat, sed diam voluptua. At vero eos et accusam et justo " +
              "duo dolores et ea rebum.";
  

  protected void setUp() throws Exception {
    initializeFtpServer();
    startFtpServer();
    Thread.sleep(100);
    createRemoteContent();
    Thread.sleep(100);
  }


  protected void tearDown() throws Exception {
    stopFtpServer();
    Thread.sleep(100);
  }


  /** Initialize FTP server */
  protected void initializeFtpServer() {
    if (launcher == null) {
      launcher = new Launcher();
      BeanFactory bf = Launcher.createClasspathXmlBeanFactory(beansFilename);
      launcher.setBeanFactory(bf);
      launcher.initialize();
    }
  }


  /** Start FTP server */
  protected void startFtpServer() throws Exception {
    if (launcher != null) launcher.startCore();
    else throw new Exception("No launcher");
  }


  /** Stop FTP server */
  protected void stopFtpServer() throws Exception {
    if (launcher != null) launcher.stopCore();
    else throw new Exception("No launcher");
  }


  /**
   * Get file factory
   * @return File factory
   */
  protected TFileFactory getFileFactory() {
    return (TFileFactory) ObjectFactory.getObject(fileFactoryName);
  }


  /** Create remote filesystem content */
  protected void createRemoteContent() throws Exception {
    TFileFactory fileFactory = getFileFactory();
    TDirectory tdir = fileFactory.dir(remoteUsersPath+"/anonymous");
    tdir.ensureExists();
  }


  /**
   * Create remote filesystem test content in user's directory
   * @param username Username of the user to create the content for
   */
  protected void createUserRemoteContent(String username) throws Exception {
    TFileFactory fileFactory = getFileFactory();
      
    fileFactory.dir(remoteUsersPath+"/"+username+"/dir1/dir1/dir1").ensureExists();
    fileFactory.dir(remoteUsersPath+"/"+username+"/dir1/dir1/dir2").ensureExists();
    fileFactory.dir(remoteUsersPath+"/"+username+"/dir1/dir1/dir3").ensureExists();
    fileFactory.dir(remoteUsersPath+"/"+username+"/dir1/dir2/dir1").ensureExists();
    fileFactory.dir(remoteUsersPath+"/"+username+"/dir1/dir2/dir2").ensureExists();
    fileFactory.dir(remoteUsersPath+"/"+username+"/dir1/dir2/dir3").ensureExists();
    fileFactory.dir(remoteUsersPath+"/"+username+"/dir1/dir3/dir1").ensureExists();
    fileFactory.dir(remoteUsersPath+"/"+username+"/dir1/dir3/dir2").ensureExists();
    fileFactory.dir(remoteUsersPath+"/"+username+"/dir1/dir3/dir3").ensureExists();

    fileFactory.dir(remoteUsersPath+"/"+username+"/dir2/dir1/dir1").ensureExists();
    fileFactory.dir(remoteUsersPath+"/"+username+"/dir2/dir1/dir2").ensureExists();
    fileFactory.dir(remoteUsersPath+"/"+username+"/dir2/dir1/dir3").ensureExists();
    fileFactory.dir(remoteUsersPath+"/"+username+"/dir2/dir2/dir1").ensureExists();
    fileFactory.dir(remoteUsersPath+"/"+username+"/dir2/dir2/dir2").ensureExists();
    fileFactory.dir(remoteUsersPath+"/"+username+"/dir2/dir2/dir3").ensureExists();
    fileFactory.dir(remoteUsersPath+"/"+username+"/dir2/dir3/dir1").ensureExists();
    fileFactory.dir(remoteUsersPath+"/"+username+"/dir2/dir3/dir2").ensureExists();
    fileFactory.dir(remoteUsersPath+"/"+username+"/dir2/dir3/dir3").ensureExists();

    fileFactory.dir(remoteUsersPath+"/"+username+"/dir3/dir1/dir1").ensureExists();
    fileFactory.dir(remoteUsersPath+"/"+username+"/dir3/dir1/dir2").ensureExists();
    fileFactory.dir(remoteUsersPath+"/"+username+"/dir3/dir1/dir3").ensureExists();
    fileFactory.dir(remoteUsersPath+"/"+username+"/dir3/dir2/dir1").ensureExists();
    fileFactory.dir(remoteUsersPath+"/"+username+"/dir3/dir2/dir2").ensureExists();
    fileFactory.dir(remoteUsersPath+"/"+username+"/dir3/dir2/dir3").ensureExists();
    fileFactory.dir(remoteUsersPath+"/"+username+"/dir3/dir3/dir1").ensureExists();
    fileFactory.dir(remoteUsersPath+"/"+username+"/dir3/dir3/dir2").ensureExists();
    fileFactory.dir(remoteUsersPath+"/"+username+"/dir3/dir3/dir3").ensureExists();

    //dir1

    createRemoteBinaryFileN(MBx1/100, remoteUsersPath+"/"+username+"/dir1/file1.dat");
    createRemoteBinaryFileN(MBx1/100, remoteUsersPath+"/"+username+"/dir1/file2.dat");
    createRemoteBinaryFileN(MBx1/100, remoteUsersPath+"/"+username+"/dir1/file3.dat");

    createRemoteBinaryFileN(MBx1/100, remoteUsersPath+"/"+username+"/dir1/dir1/file1.dat");
    createRemoteBinaryFileN(MBx1/100, remoteUsersPath+"/"+username+"/dir1/dir1/file2.dat");
    createRemoteBinaryFileN(MBx1/100, remoteUsersPath+"/"+username+"/dir1/dir1/file3.dat");

    createRemoteBinaryFileN(MBx1/100, remoteUsersPath+"/"+username+"/dir1/dir1/dir1/file1.dat");
    createRemoteBinaryFileN(MBx1/100, remoteUsersPath+"/"+username+"/dir1/dir1/dir1/file2.dat");
    createRemoteBinaryFileN(MBx1/100, remoteUsersPath+"/"+username+"/dir1/dir1/dir1/file3.dat");

    createRemoteBinaryFileN(MBx1/100, remoteUsersPath+"/"+username+"/dir1/dir1/dir2/file1.dat");
    createRemoteBinaryFileN(MBx1/100, remoteUsersPath+"/"+username+"/dir1/dir1/dir2/file2.dat");

    createRemoteBinaryFileN(MBx1/100, remoteUsersPath+"/"+username+"/dir1/dir1/dir3/file1.dat");

    createRemoteBinaryFileN(MBx1/100, remoteUsersPath+"/"+username+"/dir1/dir2/file1.dat");

    createRemoteBinaryFileN(MBx1/100, remoteUsersPath+"/"+username+"/dir1/dir2/dir1/file1.dat");
    createRemoteBinaryFileN(MBx1/100, remoteUsersPath+"/"+username+"/dir1/dir2/dir1/file2.dat");

    createRemoteBinaryFileN(MBx1/100, remoteUsersPath+"/"+username+"/dir1/dir2/dir3/file1.dat");

    createRemoteBinaryFileN(MBx1/100, remoteUsersPath+"/"+username+"/dir1/dir3/dir1/file1.dat");

    //dir2

    createRemoteBinaryFileN(MBx1/100, remoteUsersPath+"/"+username+"/dir2/file1.dat");

    createRemoteBinaryFileN(MBx1/100, remoteUsersPath+"/"+username+"/dir2/dir1/file1.dat");
    createRemoteBinaryFileN(MBx1/100, remoteUsersPath+"/"+username+"/dir2/dir1/file2.dat");

    createRemoteBinaryFileN(MBx1/100, remoteUsersPath+"/"+username+"/dir2/dir1/dir1/file1.dat");
    createRemoteBinaryFileN(MBx1/100, remoteUsersPath+"/"+username+"/dir2/dir1/dir1/file2.dat");

    createRemoteBinaryFileN(MBx1/100, remoteUsersPath+"/"+username+"/dir2/dir1/dir2/file1.dat");
    createRemoteBinaryFileN(MBx1/100, remoteUsersPath+"/"+username+"/dir2/dir1/dir2/file2.dat");

    createRemoteBinaryFileN(MBx1/100, remoteUsersPath+"/"+username+"/dir2/dir1/dir3/file1.dat");

    createRemoteBinaryFileN(MBx1/100, remoteUsersPath+"/"+username+"/dir2/dir2/file1.dat");

    createRemoteBinaryFileN(MBx1/100, remoteUsersPath+"/"+username+"/dir2/dir2/dir1/file1.dat");
    createRemoteBinaryFileN(MBx1/100, remoteUsersPath+"/"+username+"/dir2/dir2/dir1/file2.dat");

    createRemoteBinaryFileN(MBx1/100, remoteUsersPath+"/"+username+"/dir2/dir2/dir3/file1.dat");

    createRemoteBinaryFileN(MBx1/100, remoteUsersPath+"/"+username+"/dir2/dir3/dir1/file1.dat");
      
    //dir3

    createRemoteBinaryFileN(MBx1/100, remoteUsersPath+"/"+username+"/dir3/dir1/file1.dat");

    createRemoteBinaryFileN(MBx1/100, remoteUsersPath+"/"+username+"/dir3/dir1/dir1/file1.dat");
  }


    /**
     * Copies a binary file to a remote filesystem. The file must be in a classpath.
     * Note: This method should be used only in the setUp()
     * @param filename File name
     * @param directory Remote directory in an absolute form to copy the file into
     */
    protected void copyClasspathFileToRemoteFileSystem(String filename, String directory) throws Exception {
      ResourceLoader loader = new DefaultResourceLoader(getClass().getClassLoader());
      Resource r = loader.getResource(filename);
      if (r == null) throw new Exception("File not found: "+filename);

      TFileFactory fileFactory = getFileFactory();
      TFile tfile = fileFactory.file(directory+"/"+filename);
      CByte.inputToOutput(r.getInputStream(), tfile.io().outputStream(OutputMode.OVERWRITE));
      log.info("File "+tfile.toPath().toPathString()+" created, size "+tfile.length());
    }


    /**
     * Create a text file with dummy data on a remote system.
     * Note: data contains repeated symbols a,b,c,d,e,f,i,j,q,linefeed
     * @param size File size
     * @param filename File name in an absolute form
     * @param win TRUE for \r\n linefeed, FALSE for \n linefeed
     */
    protected void createRemoteTextFile(long size, String filename, boolean win) throws Exception {
      char[] carr = win ? new char[]{'a','b','c','d','e','f','i','j','\r','\n'} :
                          new char[]{'a','b','c','d','e','f','i','j','q','\n'};

      byte[] data = new byte[(int)size];
      int ind = 0;
      for (int z = 0; z < data.length; z++) {
        data[z] = (byte)carr[ind];
        if (++ind >= carr.length) ind = 0;
      }

      TFileFactory fileFactory = getFileFactory();
      TFile tfile = fileFactory.file(filename);
      CByte.inputToOutput(new ByteArrayInputStream(data), tfile.io().outputStream(OutputMode.OVERWRITE));
      log.info("File "+filename+" created, size "+size);
    }


    /**
     * Create a binary file with dummy data on a remote system.
     * Note: data contains repeated bytes [0..100]
     * @param size File size
     * @param filename File name in an absolute form
     */
    protected void createRemoteBinaryFile(long size, String filename) throws Exception {
      byte[] data = new byte[(int)size];
      byte b = 0;
      for (int z = 0; z < data.length; z++) {
        data[z] =  b;
        if (++b >= 100) b = 0;
      }

      TFileFactory fileFactory = getFileFactory();
      TFile tfile = fileFactory.file(filename);
      CByte.inputToOutput(new ByteArrayInputStream(data), tfile.io().outputStream(OutputMode.OVERWRITE));
      log.info("File "+filename+" created, size "+size);
    }


    /**
     * Same as "createRemoteBinaryFile" but does not log.
     */
    protected void createRemoteBinaryFileN(long size, String filename) throws Exception {
      byte[] data = new byte[(int)size];
      byte b = 0;
      for (int z = 0; z < data.length; z++) {
        data[z] =  b;
        if (++b >= 100) b = 0;
      }

      TFileFactory fileFactory = getFileFactory();
      TFile tfile = fileFactory.file(filename);
      CByte.inputToOutput(new ByteArrayInputStream(data), tfile.io().outputStream(OutputMode.OVERWRITE));
    }


    /**
     * Replace linefeeds (\r\n) with unix linefeeds (\n)
     * @param str String to replace linefeeds in
     * @return Converted string
     */
    protected String convertToUnixLineFeeds(String str) {
      StringReaper sr = new StringReaper(str);
      sr.replace("\r", "");
      return sr.getContent();
    }


    /**
     * Read file from a remote system
     * @param filename File name in an absolute form
     * @return File data
     */
    protected byte[] readRemoteFile(String filename) throws Exception {
      TFileFactory fileFactory = getFileFactory();
      TFile tfile = fileFactory.file(filename);
      return CByte.toByteArray(tfile.io().inputStream());
    }


    /**
     * Get speed
     * @param mbs Mebabytes transferred
     * @param mills Mills spent
     * @return Speed (bytes/sec)
     */
    protected long mills2bps(long mbs, long mills) {
      return (long)((double)mbs/((double)mills/1000d));
    }

}