package com.coldcore.coloradoftp.testpack.test.concurrent;

import com.coldcore.coloradoftp.testpack.core.RandomClient;
import com.coldcore.coloradoftp.testpack.core.RandomCustomClient;

import org.apache.log4j.Logger;

import java.util.Set;

/**
 * Bease test class.
 * All clients connect at the same time then eventually disconnect and connect again.
 */
abstract public class ConcurrentDynamicBase extends ConcurrentBase {

    private static Logger log = Logger.getLogger(ConcurrentDynamicBase.class);
    protected boolean fail = false;


    protected void setUp() throws Exception {
        super.setUp();
        fail = false;
    }



    /*** Methods to use in tests ***/


    /** Same as checkErrors but does not fail if errors detected */
    protected void checkErrorsNoFail() {
        for (RandomCustomClient c : clients) {
            Set<Throwable> errors = c.getErrors();
            for (Throwable e : errors) {
                log.error("Client '"+c.getUsername()+"' ERROR: "+e.getMessage());
                //log.error(e);  //Print stack trace
                fail = true;
            }
        }
    }


    protected void checkErrors() {
        if (fail) fail("Client errors!");
    }


    /**
     * Wait eventually disconnecting and reconnections clients
     * @param seconds How many seconds to wait
     */
    protected void waitFor(int seconds) throws Exception {
        long end = System.currentTimeMillis()+(long)seconds*1000L;
        for (int z = 0; z < seconds; z++) {
            long cur = System.currentTimeMillis();
            int alive = countAliveClients();
            if (z%10 == 0) log.info((end-cur)/1000L+" seconds remaining ("+alive+" alive clients)");
            if (cur >= end) break;
            Thread.sleep(1000);

            checkErrorsNoFail();

            int index = 0;
            for (RandomCustomClient c : clients) {
                int r = (int)(Math.random()*(double)30)+1;
                if (!c.isRunning() && cur-c.getStopped() > 5000L) { //Rest 5 seconds! VM must free ports etc or the reconnected client will become bugged
                    c.start();
                    //log.info("Client '"+c.getUsername()+"' started");
                } else if (r == 1 && cur-c.getStarted() > 5000L) { //Approx 1 disconnec per 2 seconds, client runs at least 5 seconds
                    c.stop();
                    //log.info("Client '"+c.getUsername()+"' stopped");
                }
                index++;
            }
        }
    }

}