package com.coldcore.coloradoftp.betatest;

import net.sf.cotta.TFile;
import org.apache.commons.net.ftp.FTPClient;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;

/**
 * This is an example of a simple test file.
 */
public class SimpleTest extends BaseTest {

  private String lorem = "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, " +
          "sed diam nonumy eirmod tempor invidunt ut labore et dolore magna " +
          "aliquyam erat, sed diam voluptua. At vero eos et accusam et justo " +
          "duo dolores et ea rebum.";


  protected void setUp() throws Exception {
    super.setUp();
    loginFtpClient();
  }


  public void testLoginAndLogout() throws Exception {
    assertTrue(ftpClient.logout());
  }


  public void testUploadFile() throws Exception {
    assertTrue(ftpClient.setFileType(FTPClient.BINARY_FILE_TYPE));
    assertTrue(ftpClient.storeFile("lorem.txt", new ByteArrayInputStream(lorem.getBytes())));

    TFile tfile = getFileFactory().file(remoteUsersPath+"/anonymous/lorem.txt");
    assertTrue(tfile.exists());
    assertEquals(lorem, tfile.load());
  }


  public void testDownloadFile() throws Exception {
    TFile tfile = getFileFactory().file(remoteUsersPath+"/anonymous/lorem.txt");
    tfile.save(lorem);

    ByteArrayOutputStream bout = new ByteArrayOutputStream();
    assertTrue(ftpClient.setFileType(FTPClient.BINARY_FILE_TYPE));
    assertTrue(ftpClient.retrieveFile("lorem.txt", bout));

    assertEquals(lorem, new String(bout.toByteArray()));
  }

}
